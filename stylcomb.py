import sublime
import sublime_plugin
import os
import json
import platform
from subprocess import Popen, PIPE

# monkeypatch `Region` to be iterable
sublime.Region.totuple = lambda self: (self.a, self.b)
sublime.Region.__iter__ = lambda self: self.totuple().__iter__()

COMB_PATH = os.path.join(sublime.packages_path(), os.path.dirname(os.path.realpath(__file__)), 'csscomb.js')
CSS2STYLUS_PATH = os.path.join(sublime.packages_path(), os.path.dirname(os.path.realpath(__file__)), 'css2stylus.js')
CSSCOMB_SETTINGS_PATH = os.path.join(sublime.packages_path(), os.path.dirname(os.path.realpath(__file__)), 'csscomb-settings.json')



def active_view():
    return sublime.active_window().active_view()

def get_settings():
    view = active_view()
    settings = view.settings().get('stylcomb')
    if settings is None:
        settings = sublime.load_settings('stylcomb.sublime-settings')
    return settings

def get_special_mark():
    return get_settings().get('nocomb_mark')

class MarkLineCommand(sublime_plugin.TextCommand):
    def is_marked(self, row):
        region = self.view.find(mark, self.view.text_point(row, 0))
        return not region.empty()

    def has_selection(self):
        for sel in self.view.sel():
            start, end = sel
            if start != end:
                return True
        return False

    def process_line(self, edit, row):
        indent_region = self.view.find('^\s+', self.view.line(self.view.sel()[0].begin()).begin())
        indent_size = indent_region.size()
        if self.is_marked(row):
            region = self.view.line(self.view.text_point(row, 0))
            text = self.view.substr(region).replace(mark, "")
            self.view.replace(edit, region, text)
        else:
            self.view.insert(edit, self.view.text_point(row, indent_size), mark)

    def run(self, edit):
        globals()['mark'] = get_special_mark()
        if self.has_selection():
            for region in self.view.sel():
                lines = self.view.lines(region)
                rows = []
                for line in lines:
                    row, col = self.view.rowcol(line.begin())
                    rows.append(row)
                for row in rows:
                    self.process_line(edit, row)

            return
        row, col = self.view.rowcol(self.view.line(self.view.sel()[0]).begin())
        self.process_line(edit, row)
        return

class StylcombCommand(sublime_plugin.TextCommand):
    def toCss(self, css):
        try:
            p = Popen(['stylus', '-c'],
                stdout=PIPE, stdin=PIPE, stderr=PIPE,
                env=self.get_env(), shell=self.is_windows())
        except OSError:
            raise Exception("Couldn't find stylus. Make sure that stylus install globally")
        stdout, stderr = p.communicate(input=css.encode('utf-8'))
        if stdout:
            return stdout.decode('utf-8')
        else:
            sublime.error_message('CSS error:\n%s' % stderr.decode('utf-8'))

    def toStylus(self, css):
        folder = os.path.dirname(self.view.file_name())
        try:
            p = Popen(['node', CSS2STYLUS_PATH] + [folder],
                stdout=PIPE, stdin=PIPE, stderr=PIPE,
                env=self.get_env(), shell=self.is_windows())
        except OSError:
            raise Exception("Couldn't find Node.js. Make sure it's in your " +
                            '$PATH by running `node -v` in your command-line.')
        stdout, stderr = p.communicate(input=css.encode('utf-8'))
        if stdout:
            return stdout.decode('utf-8')
        else:
            sublime.error_message('Css2stylus error:\n%s' % stderr.decode('utf-8'))

    def comb(self, css, syntax, config):
        config = json.dumps(config)
        folder = os.path.dirname(self.view.file_name())
        try:
            p = Popen(['node', COMB_PATH] + [syntax, config, folder],
                stdout=PIPE, stdin=PIPE, stderr=PIPE,
                env=self.get_env(), shell=self.is_windows())
        except OSError:
            raise Exception("Couldn't find Node.js. Make sure it's in your " +
                            '$PATH by running `node -v` in your command-line.')
        stdout, stderr = p.communicate(input=css.encode('utf-8'))
        if stdout:
            return stdout.decode('utf-8')
        else:
            sublime.error_message('CSScomb error:\n%s' % stderr.decode('utf-8'))

    def get_csscomb_config(self):
        with open(CSSCOMB_SETTINGS_PATH, encoding='utf-8') as data_file:
            config = json.load(data_file)
        return config

    def get_env(self):
        env = None
        if self.is_osx():
            env = os.environ.copy()
            env['PATH'] += self.get_node_path()
        return env

    def get_node_path(self):
        return get_settings().get('node-path')

    def is_osx(self):
        return platform.system() == 'Darwin'

    def is_windows(self):
        return platform.system() == 'Windows'

    def is_stylus(self):
        return self.view.scope_name(0).startswith('source.stylus')

    def has_selection(self):
        for sel in self.view.sel():
            start, end = sel
            if start != end:
                return True
        return False

    # def reduce_extend_line(self, text):
    #     lines = text.splitlines()
    #     extends_arr = []
    #     for i, line in enumerate(lines):
    #         if line.find('@extend') != -1:
    #             extends_arr.append(line)
    #             lines[i] = line[:line.find('@extend')] + "stylComb_extend: %s" % (len(extends_arr) - 1)
    #     result = "\n".join(lines)
    #     return (result, extends_arr)

    def process_extend(self, text):
        text_lines = text.splitlines()
        for i, line in enumerate(text_lines):
            if line.find('@extend') != -1:
                text_lines[i] = line[:line.find('@extend')] + mark + line[line.find('@extend'):]
        result = "\n".join(text_lines)
        return result

    def reduce_nocomb_line(self, text):
        text_lines = text.splitlines()
        nocomb_arr = []
        for i, line in enumerate(text_lines):
            if line.find(mark) != -1:
                text = line.replace(mark, "")
                nocomb_arr.append(text)
                text_lines[i] = line[:line.find(mark)] + "stylComb_nocomb: %s" % (len(nocomb_arr) - 1)
        result = "\n".join(text_lines)
        return (result, nocomb_arr)

    # def expand_extend_line(self, text, extends_arr):
    #     lines = text.splitlines()
    #     for i, line in enumerate(lines):
    #         if line.find('stylComb_extend') != -1:
    #             index = int(line.strip()[-1])
    #             lines[i] = extends_arr[index]
    #     result = "\n".join(lines)
    #     return result

    def expand_nocomb_line(self, text, nocomb_arr):
        text_lines = text.splitlines()
        for i, line in enumerate(text_lines):
            if line.find('stylComb_nocomb') != -1:
                index = int(line.strip()[-1])
                text_lines[i] = nocomb_arr[index]
        result = "\n".join(text_lines)
        return result

    def run(self, edit):
        if not self.is_stylus():
            return
        globals()['mark'] = get_special_mark()
        csscomb_config = self.get_csscomb_config()
        if not self.has_selection():
            region = sublime.Region(0, self.view.size())
            text = self.view.substr(region)
            text = self.process_extend(text)
            text, nocomb_arr = self.reduce_nocomb_line(text)
            cssed = self.toCss(text)
            combed = self.comb(cssed, 'css', csscomb_config)
            stylused = self.toStylus(combed)
            stylused = self.expand_nocomb_line(stylused, nocomb_arr)
            if stylused:
                self.view.replace(edit, region, stylused)
            return

        for region in self.view.sel():
            if region.empty():
                continue
            text = self.view.substr(region)
            text = self.process_extend(text)
            text, nocomb_arr = self.reduce_nocomb_line(text)
            cssed = self.toCss(text)
            combed = self.comb(cssed, 'css', csscomb_config)
            stylused = self.toStylus(combed)
            stylused = self.expand_nocomb_line(stylused, nocomb_arr)
            if stylused:
                self.view.replace(edit, region, stylused)
        return





