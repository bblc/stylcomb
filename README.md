# stylComb for Sublime Text

## Installation

Clone this repo to your package location (Preference -> Browse Packages...)

## About

stylComb is a coding style formatter ([CSScomb](https://github.com/csscomb/csscomb.js)) for Stylus files. Inspired by [sublime-csscomb](https://github.com/csscomb/sublime-csscomb)

## The Requirements

You need [Stylus](https://github.com/stylus/stylus) to make this plugin work.

### Caveats

If node has been installed with NVM you need to make a symlink to node in `/usr/local/bin`. Using OS X, the binary path would typically be `/Users/[your name]/.nvm/[node version]/bin/node`.

## Plugin usage

Press **ctrl+shift+c** or open "Tools" menu and select "Run stylComb".

Press **ctrl+shift+/** for mark line that won't be combed.
