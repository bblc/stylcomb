var str = '';

process.stdin.resume();
process.stdin.setEncoding('utf8');

process.stdin.on('data', function (data) {
    str += data;
});

process.stdin.on('end', function () {
    var Css2stylus = require('./node_modules/css2stylus/lib/css2stylus'),
        css2stylus = new Css2stylus.Converter(str);

    process.chdir(process.argv[2]);

    css2stylus.processCss({
      indent: '4',
      keepColons: true
    });
    stylused = css2stylus.getStylus();
    process.stdout.write(stylused);
});
